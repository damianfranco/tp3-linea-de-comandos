# Comandos utilizados en esta practica

### Inciso A) y C)
 
-Este comando lo use para realizar los incisos A) y C) utilizando la consola de powershell.

Get-ChildItem -Filter "*mid_*" -Recurse | Rename-Item -NewName {$_.name -replace "mid_","img_" }

### Inciso B)

-Para inciso  B) utilize el comando

ls -l> renameFake.dat

### Inciso D)

-Para inciso  D) utilize el comando

ls -l> renameReal.dat

### Inciso E)

-En el inciso E) cree una carpeta con el comando

mkdir dataSet

### Inciso F)

-Para el inciso F) movi las cosas de a partes con el comando 

mv fake/*.dat dataSet
mv fake/*.jpg dataSet
mv real/*.dat dataSet
mv real/*.jpg dataSet

### Inciso G)

-Para el inciso G) investigue el comando tar utilizando --help porque en windows no funcionaba el comando man.

Para comprimir los archivos utilize el comando 

tar cvf dataSet.tar img_* renameFake.dat renameReal.dat

Para ver el contenido del archivo comprimido utilize el comando

tar tvf dataSet.tar

Y para descomprimir el archivo utilize el comando 

tar xvf dataSet.tar